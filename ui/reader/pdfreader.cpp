/* Copyright 2015 Robert Schroll
 * Copyright 2021 Emanuele Sorce <emanuele.sorce@hotmail.com>
 *
 * This file is part of Beru and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */

#include "pdfreader.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QtGui/QImage>
#include <QBuffer>
#include <QDir>
#include <QByteArray>
#include <QString>
#include <QDebug>
#include <QCryptographicHash>

#include <QtConcurrent>

#include "../qhttpserver/qhttpresponse.h"
#include "../mimetype.h"

// TODO: make this configurable
#define MAX_PAGES_IN_CACHE 5

PDFReader::PDFReader(QObject *parent) : QObject(parent),
										rendering_count(0),
										pdf(NULL)
{
}

PDFReader::~PDFReader()
{
	if (pdf != NULL)
	{
		delete pdf;
		pdf = NULL;
	}
}

void PDFReader::renderingCountIncrease()
{
	rendering_count_mutex.lock();
	++rendering_count;
	rendering_count_mutex.unlock();
}
void PDFReader::renderingCountDecrease()
{
	rendering_count_mutex.lock();
	--rendering_count;
	rendering_count_mutex.unlock();
}

bool PDFReader::load(const QString &filename)
{
	unload();
	pdf = Poppler::Document::load(filename.toUtf8());
	if (pdf == NULL)
		return false;
	if (!parse())
	{
		delete pdf;
		pdf = NULL;
		return false;
	}
	computeHash(filename);
	readMetadata();
	pdf->setRenderHint(Poppler::Document::Antialiasing, true);
	pdf->setRenderHint(Poppler::Document::TextAntialiasing, true);

	cache_mutex.lock();
	pagecache.clear();
	pagecache_ready.clear();
	pagecache.resize(numberOfPages());
	pages_rendering.clear();
	cache_mutex.unlock();

	return true;
}
void PDFReader::unload()
{
	// wait for all rendering to finish
	QEventLoop loop;
	QTimer t;
	while (rendering_count != 0)
	{
		qDebug() << "Previous pdf is still rendering, waiting a bit...";
		t.connect(&t, &QTimer::timeout, &loop, &QEventLoop::quit);
		t.start(500);
		loop.exec();
	}

	if (pdf != NULL)
	{
		delete pdf;
		pdf = NULL;
	}
	spine.clear();
	metadata.clear();
}

bool PDFReader::parse()
{
	if (pdf == NULL)
		return false;
	int n = pdf->numPages();
	for (int i = 0; i < n; i++)
		spine.append(QString::number(i + 1));
	return true;
}

void PDFReader::readMetadata()
{
	QStringList keys = pdf->infoKeys();
	foreach (QString k, keys)
	{
		QString value = pdf->info(k);
		if (value != "")
			metadata[k.toLower()] = value;
	}
}

QString PDFReader::hash()
{
	return _hash;
}

void PDFReader::computeHash(const QString &filename)
{
	// Doing a MD5 hash of the whole file can take a while, so we only
	// do the first 10 kB.  Hopefully that's enough to be unique.
	QFile file(filename);
	if (file.open(QFile::ReadOnly))
	{
		QByteArray data = file.read(10 * 1024);
		QCryptographicHash hash(QCryptographicHash::Md5);
		hash.addData(data);
		_hash = hash.result().toHex();
	}
}

QString PDFReader::title()
{
	return metadata.contains("title") ? metadata["title"].toString() : "";
}

int PDFReader::height()
{
	return _height;
}

void PDFReader::setHeight(int value)
{
	if (value > 0)
	{
		_height = value;
		// clear cache
		cache_mutex.lock();
		pagecache_ready.clear();
		pagecache.clear();
		cache_mutex.unlock();
	}
}

int PDFReader::width()
{
	return _width;
}

void PDFReader::setWidth(int value)
{
	if (value > 0)
	{
		_width = value;
		// clear cache
		cache_mutex.lock();
		pagecache_ready.clear();
		pagecache.clear();
		cache_mutex.unlock();
	}
}

// ?? Sturm reader standard is first page is 1, so pagenum must be reduced by one when this function is called ??
QByteArray PDFReader::renderPage(int pageNum, QString hash)
{
	// pdf document has changed in the meantime!
	if (pdf == NULL || _hash != hash)
	{
		return QByteArray();
	}

	renderingCountIncrease();

	// cache hit?
	cache_mutex.lock();
	if (pagecache_ready.contains(pageNum))
	{
		QByteArray temp = pagecache[pageNum];
		cache_mutex.unlock();
		renderingCountDecrease();
		return temp;
	}

	// already rendering this page
	if (pages_rendering.contains(pageNum))
	{
		// wait for it to finish
		QEventLoop loop;
		QTimer t;
		while (!pagecache_ready.contains(pageNum))
		{
			cache_mutex.unlock();
			t.connect(&t, &QTimer::timeout, &loop, &QEventLoop::quit);
			t.start(100);
			loop.exec();
			cache_mutex.lock();
		}
		QByteArray temp = pagecache[pageNum];
		// return it from cache
		cache_mutex.unlock();
		renderingCountDecrease();
		return temp;
	}
	pages_rendering.append(pageNum);
	cache_mutex.unlock();

	// FIXME: this should be done better, not trigger a whole rendering for nothing
	if (pageNum < 0 || pageNum >= pdf->numPages())
	{
		qDebug() << "WARN! Requested rendering of page out of range: " << pageNum;
		pageNum = 0;
	}

	// where to write the image
	QByteArray byteArray;
	QBuffer buffer(&byteArray);
	buffer.open(QIODevice::WriteOnly);

	Poppler::Page *page = pdf->page(pageNum);

	// page size in points
	QSizeF pageSize = page->pageSizeF();

	// page size in inches
	qreal pageWidth = pageSize.width() / 72;
	qreal pageHeight = pageSize.height() / 72;

	double res;

	if ((double)pageWidth / pageHeight > (double)_width / _height)
		res = _height / pageHeight;
	else
		res = _width / pageWidth;

	QImage img(page->renderToImage(res, res));
	delete page;

	img.save(&buffer, "PNG");

	// save to cache
	pages_rendering.removeOne(pageNum);
	cache_mutex.lock();
	if (!pagecache_ready.contains(pageNum))
	{
		pagecache[pageNum] = byteArray;
		pagecache_ready.append(pageNum);
	}
	cache_mutex.unlock();

	renderingCountDecrease();
	return byteArray;
}

int PDFReader::numberOfPages()
{
	if (!pdf)
	{
		qDebug() << "PDF file not open";
		return 0;
	}
	return pdf->numPages();
}

void PDFReader::serveComponent(int pagenum, QHttpResponse *response)
{
	if (!pdf)
	{
		response->writeHead(500);
		response->end("PDF file not open for reading");
		return;
	}
	renderingCountIncrease();

	response->setHeader("Content-Type", guessMimeType("png"));
	response->writeHead(200);

	QFutureWatcher<QByteArray> *watcher = new QFutureWatcher<QByteArray>();
	QFuture<QByteArray> *page = new QFuture<QByteArray>();

	connect(watcher, &QFutureWatcher<QByteArray>::finished, this, [this, response, watcher, page, pagenum]
			{ finishServeComponent(response, watcher, page, pagenum); });

	*page = QtConcurrent::run(this, &PDFReader::renderPage, pagenum - 1, _hash);
	watcher->setFuture(*page);
}

void PDFReader::finishServeComponent(QHttpResponse *response, QFutureWatcher<QByteArray> *watcher, QFuture<QByteArray> *page, int pagenum)
{
	renderingCountDecrease();

	response->write(page->result());
	response->end();

	delete watcher;
	delete page;

	// clean cache?
	cache_mutex.lock();
	while (pagecache_ready.size() > MAX_PAGES_IN_CACHE)
	{
		// clear older pages
		int clearpage = pagecache_ready.takeFirst();
		pagecache[clearpage].clear();
	}
	cache_mutex.unlock();

	// Make sure next and previous page are in cache
	QtConcurrent::run(this, &PDFReader::renderPage, pagenum + 1 - 1, _hash);
	QtConcurrent::run(this, &PDFReader::renderPage, pagenum - 1 - 1, _hash);
}

QVariantList PDFReader::getContents()
{
	QVariantList res;
	if (!pdf)
		return res;
	QDomDocument *toc = pdf->toc();
	if (toc)
	{
		res = parseContents(toc->firstChildElement());
		delete toc;
	}
	emit contentsReady(res);
	return res;
}

QVariantList PDFReader::parseContents(QDomElement el)
{
	QVariantList res;
	if (!pdf)
		return res;
	while (!el.isNull())
	{
		QString title = el.tagName();
		Poppler::LinkDestination *destination = NULL;
		if (el.hasAttribute("Destination"))
		{
			destination = new Poppler::LinkDestination(el.attribute("Destination"));
		}
		else if (el.hasAttribute("DestinationName"))
		{
			destination = pdf->linkDestination(el.attribute("DestinationName"));
		}
		if (destination)
		{
			QVariantMap entry;
			entry["title"] = title;
			entry["src"] = QString::number(destination->pageNumber());
			QDomElement child = el.firstChildElement();
			if (!child.isNull())
				entry["children"] = parseContents(child);
			res.append(entry);
			delete destination;
		}
		el = el.nextSiblingElement();
	}
	return res;
}

QVariantMap PDFReader::getCoverInfo(int thumbsize, int fullsize)
{
	QVariantMap res;
	if (!pdf)
		return res;

	res["title"] = metadata.contains("title") ? metadata["title"] : "ZZZnone";
	res["author"] = metadata.contains("author") ? metadata["author"] : "";
	res["authorsort"] = "zzznone";
	res["cover"] = "ZZZnone";

	// 'subject' in PDF is intended as description
	res["description"] = metadata.contains("subject") ? metadata["subject"] : "";
	res["subject"] = metadata.contains("keywords") ? metadata["keywords"] : "";

	// not sure language exists, but why not
	res["language"] = metadata.contains("language") ? metadata["language"] : "";

	int original_width = _width;
	int original_height = _height;
	_width = _height = thumbsize;
	QByteArray byteArray = renderPage(0, _hash);
	res["cover"] = "data:image/png;base64," + QString(byteArray.toBase64());

	_width = _height = fullsize;
	QByteArray byteArrayf = renderPage(0, _hash);
	res["fullcover"] = "data:image/png;base64," + QString(byteArrayf.toBase64());

	_width = original_width;
	_height = original_height;

	return res;
}
