/* Copyright 2013 Robert Schroll
 * Copyright 2021 Emanuele Sorce
 *
 * This file is part of Beru and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */

#include "fileserver.h"
#include "qhttpserver/qhttpresponse.h"
#include "mimetype.h"
#include <QFile>
#include <QDebug>
#include <QElapsedTimer>

void FileServer::serve(const QString &filename, QHttpResponse *response)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
		qDebug() << "404 - file not found: " << filename;
		response->writeHead(404);
		response->setHeader("Accept-Ranges", "Bytes");
        response->end("File not found");
        return;
    }

    response->setHeader("Content-Type", guessMimeType(filename));
	response->setHeader("Content-Length", QString::number(file.size()) );
    response->writeHead(200);

	// write book data in chunks

	// profiling
	//QElapsedTimer timer;
	//timer.start();

	int chunk_size = qMin(file.size(), qint64(1024 * 1024 * 16));
	char *chunk = new char[chunk_size + 1];
	qint64 step = 1;
	//int bookdata_cursor = 0;

	while(true) {
		step = file.read(chunk, chunk_size);

		if(step > 0) {
			//bookdata_cursor += step;
			response->write( QByteArray(chunk, step) );
			response->flush();
			//qDebug() << "Sent: " << bookdata_cursor << " bytes";
		} else break;
	}
	delete[] chunk;

	response->end();

	qDebug() << "Sent file " << filename << " - " << file.size() << " bytes"; // Profiling (0.00001 + timer.elapsed() * 1024.0 * 1024 / 1000) << " Mbytes/sec";
}
