 /*
 * Copyright 2020-2021 Emanuele Sorce - emanuele.sorce@hotmail.com
 *
 * This file is part of Beru and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Dialog {
	id: content
	width: Math.min(parent.width, scaling.dp(750))
	height: Math.max(parent.height * 0.75, Math.min(parent.height, scaling.dp(500)))
	y: (parent.height - height) * 0.5
	x: (parent.width - width) * 0.5
	dim: true

	property alias pdf_newPage: pageSlider.value

	header: Column {
		width: parent.width
		ToolBar {
			width: parent.width
			RowLayout {
				anchors.fill: parent
				Label {
					text: gettext.tr("Contents")
					font.pixelSize: headerTextSize
					elide: Label.ElideRight
					horizontalAlignment: Qt.AlignHCenter
					verticalAlignment: Qt.AlignVCenter
					Layout.fillWidth: true
				}
			}
		}
		TabBar {
			id: sorttabs
			width: parent.width
			TabButton {
				text: gettext.tr("Outline")
				onClicked: {
					contentSwipe.currentIndex = 0;
					content.standardButtons = Dialog.Cancel;
				}
			}
			TabButton {
				text: gettext.tr("Pages")
				visible: pictureBook
				onClicked: {
					contentSwipe.currentIndex = 1;
					content.standardButtons = Dialog.Cancel | Dialog.Ok;
				}
			}
			TabButton {
				text: gettext.tr("Bookmarks")
				onClicked: {
					contentSwipe.currentIndex = 2;
					content.standardButtons = Dialog.Cancel;
				}
			}
		}
	}

	standardButtons: Dialog.Cancel

	SwipeView {
		id: contentSwipe
		anchors.fill: parent

		interactive: false
		clip: true

		Item {
			id: outlineLoader

			ListView {
				id: contentsListView
				anchors.fill: parent
				visible: contentsListModel.count > 0

				// TODO:
				// for the chapter, scroll throught the chapters to find the right one if any
				// 	let chap = -1;
				// 	for(let i=0; i<outline.length; i++) {
				// 		if(outline[i].src <= pageNumber)
				// 			chap = outline[i].src;
				// 		else
				// 			break;
				// 	}

				model: contentsListModel
				delegate: ItemDelegate {
					width: parent.width
					highlighted: bookPage.currentChapter == model.src
					text: (new Array(model.level + 1)).join("    ") +
							model.title.replace(/(\n| )+/g, " ").replace(/^%PAGE%/, gettext.tr("Page"))
					onClicked: {
						if(pictureBook)
							moveToLocus({pageNumber: model.src})
						else
							moveToLocus(model.src, "moveToChapter");
						content.close();
					}
				}

				Connections {
					target: bookPage
					onContentOpened: {
						for (var i=0; i<contentsListModel.count; i++) {
							if (contentsListModel.get(i).src == bookPage.currentChapter) {
								contentsListView.positionViewAtIndex(i, ListView.Center);
								break;
							}
						}
					}
				}
				ScrollBar.vertical: ScrollBar {}
			}
			Label {
				anchors.centerIn: parent
				visible: contentsListModel.count == 0
				text: gettext.tr("No outline available")
			}
		}

		Item {
			id: pagesLoader
			visible: pictureBook

			property alias slider: pageSlider

			Column {
				width: parent.width
				anchors.leftMargin: scaling.dp(10)
				anchors.rightMargin: scaling.dp(10)

				spacing: scaling.dp(15)

				Connections {
					target: content

					onOpened: {
						pagesLoader.slider.value = pdf_pageNumber;
					}
				}

				Label {
					width: parent.width
					horizontalAlignment: Text.AlignHCenter
					text: gettext.tr("Page") + " " + pagesLoader.slider.value + "/" + (pdf_numberOfPages)
					font.pixelSize: scaling.dp(16)
				}
				RowLayout {
					width: parent.width
					Button {
						Layout.alignment: Qt.AlignLeft
						text: "-"
						font.pixelSize: scaling.dp(16)
						onClicked: pagesTumbler.currentIndex -= 1
					}
					Tumbler {
						Layout.alignment: Qt.AlignHCenter
						id: pagesTumbler
						rotation: -90
						wrap: false
						model: pagesTumblerModel
						delegate: Label {
							text: model.num
							rotation: 90
							font.weight: (model.num == pagesTumbler.currentIndex+1) ? Font.Bold : Font.Normal
							font.pixelSize: (model.num == pagesTumbler.currentIndex+1) ? scaling.dp(16) : scaling.dp(13)
							width: scaling.dp(60)
							height: scaling.dp(60)
							horizontalAlignment: Text.AlignHCenter
							verticalAlignment: Text.AlignVCenter
						}
						onCurrentIndexChanged: {
							if (pagesLoader.slider.value != currentIndex+1)
								pagesLoader.slider.value = currentIndex+1;
						}
					}
					Button {
						Layout.alignment: Qt.AlignRight
						text: "+"
						font.pixelSize: scaling.dp(16)
						onClicked: pagesTumbler.currentIndex += 1
					}
				}
			}
			RowLayout {
				id: sliderRow
				width: parent.width
				anchors.bottom: parent.bottom
				Slider {
					id: pageSlider
					Layout.fillWidth: true
					from: 1
					to: pdf_numberOfPages
					stepSize: 1
					value: pdf_pageNumber
					onValueChanged: {
						if (pagesTumbler.currentIndex != value-1)
							pagesTumbler.currentIndex = value-1;
					}
					snapMode: Slider.SnapAlways
				}
				Label {
					width: scaling.dp(50)
					text: Math.floor(100 * pageSlider.value / pdf_numberOfPages) + "%"
				}
			}
		}

		Item {
			id: bookmarkView

			ListView {
				anchors.fill: parent

				// this to trigger localization of below strings
				model: ListModel {
					ListElement {
						color: "red"
						setting_save_key: "bookmark_0_locus"
					}
					ListElement {
						color: "green"
						setting_save_key: "bookmark_1_locus"
					}
					ListElement {
						color: "purple"
						setting_save_key: "bookmark_2_locus"
					}
					ListElement {
						color: "yellow"
						setting_save_key: "bookmark_3_locus"
					}
					ListElement {
						color: "blue"
						setting_save_key: "bookmark_4_locus"
					}
				}

				delegate: SwipeDelegate {
					id: bookmarkDelegate
					width: parent.width
					spacing: scaling.dp(5)

					swipe.left: Button {
						height: parent.height
						width: parent.width * 0.5
						anchors.left: parent.left
						text: gettext.tr("<font color='red'>Delete</font>")
						onClicked: {
							setBookSetting(model.setting_save_key, undefined)
							swipe.close();
						}
					}

					swipe.right: Button {
						height: parent.height
						width: parent.width * 0.5
						anchors.right: parent.right
						highlighted: true
						text: gettext.tr("<b>Set at current page</b>")
						onClicked: {
							setBookSetting(model.setting_save_key, {
								// monocle
								componentId: book_componentId,
								percent: Number(book_percent),
								// pdfjs
								pageNumber: pdf_pageNumber
							})
							swipe.close();
						}
					}

					Icon {
						anchors.right: background.right
						anchors.top: parent.top
						anchors.bottom: parent.bottom
						anchors.margins: scaling.dp(2)

						name: "tag"
						color: model.color
						width: height
					}

					// go
					onClicked: {
						var locus = getBookSetting(model.setting_save_key)
						if(locus !== undefined) {
							moveToLocus( locus );
							content.close()
						}
					}

					function generateName() {

						text = gettext.tr("Bookmark") + " " + gettext.tr(model.color) + " | "

						var locus = getBookSetting(model.setting_save_key)
						if(locus !== undefined) {
							if (pictureBook)
								text = text + gettext.tr("Page") + " " + locus.pageNumber
							else
								text = text + locus.componentId.split("/").pop().split(".").shift() + " - " +
									Math.round(locus.percent * 100) + "%"
						}
						else
							text = text + gettext.tr("(Empty)")
					}

					// Load names
					Timer {
						id: nameTimer
						interval: 500
						repeat: true
						running: bookmarkView.visible
						triggeredOnStart: true
						onTriggered: {
							generateName()
						}
					}
				}
			}
		}

		Item {
			id: searchView
			visible: false

			ColumnLayout {
				spacing: scaling.dp(5)
				anchors.fill: parent

				Label {
					width: parent.width
					text: gettext.tr("Search")
					Layout.alignment: Qt.AlignHCenter
					font.pixelSize: scaling.dp(16)
				}
			}
		}
	}

	onAccepted: {
		moveToLocus( { pageNumber: pdf_newPage } );
	}
}
