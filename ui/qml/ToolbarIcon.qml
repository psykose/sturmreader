 /* Copyright 2021 Emanuele Sorce emanuele.sorce@hotmail.com
 * 
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */ 

import QtQuick 2.12
import QtQuick.Controls 2.5

ToolButton {
	id: root

	property string name: ""
	property color color: colors.item

	padding: scaling.dp(7)
	contentItem: Icon {
		anchors.centerIn: parent
		name: root.name
		color: root.color
	}
}
