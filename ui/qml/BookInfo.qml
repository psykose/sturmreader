/*
 * Copyright 2022 Emanuele Sorce
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GNU GPL.v3 See the file COPYING for full details.
 */
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Page {
	id: bookInfo

	property var bookdata: ({ title: "", filename: "", author: "", cover: "", fullcover: "", subject: "", description: "", language: "" })
	property var coverOnSide: width > height

	onBookdataChanged: {
		if(bookdata) {
			authorLabel.text = bookdata.author !== "" ? bookdata.author : gettext.tr("Unknown author")
			titleLabel.text = bookdata.title !== "" ? bookdata.title : gettext.tr("Unknown title")
			filenameLabel.text = bookdata.filename
			languageLabel.text = (bookdata.language && bookdata.language !== "") ? bookdata.language : gettext.tr("Unknown")
			subjectLabel.text = (bookdata.subject && bookdata.subject !== "") ? bookdata.subject : gettext.tr("Unknown")
			descriptionLabel.text = (bookdata.description && bookdata.description !== "") ? bookdata.description : gettext.tr("Unknown")
		}
	}

	header: Column {
		width: parent.width
		ToolBar {
			width: parent.width
			RowLayout {
				spacing: scaling.dp(10)
				anchors.fill: parent

				ToolbarIcon {
					name: "go-previous"
					onClicked: pageStack.pop()
				}

				Label {
					id: titleLabel
					font.pixelSize: headerTextSize
					elide: Label.ElideRight
					horizontalAlignment: Qt.AlignLeft
					verticalAlignment: Qt.AlignVCenter
					Layout.fillWidth: true
				}
			}
		}
	}

	Flickable {
		id: flickable
		anchors.fill: parent
		contentHeight:  content.height + scaling.dp(80)
		contentWidth: parent.width
		ScrollBar.vertical: ScrollBar { }

		Row {
			id: content
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.right: parent.right
			anchors.margins: scaling.dp(15)

			spacing: scaling.dp(15)

			CoverDelegate {
				visible: coverOnSide

				id: infoCover

				cover: bookdata ? bookdata.cover : null
				fullcover: bookdata ? bookdata.fullcover : null

				width: parent.width / 3
				height: parent.width / 2
				title: titleLabel.text
				author: bookdata ? bookdata.author : null
			}

			Column {
				id: infoColumn
				width: coverOnSide ? parent.width / 2 : parent.width
				anchors.leftMargin: scaling.dp(5)
				anchors.rightMargin: scaling.dp(5)

				spacing: scaling.dp(20)

				CoverDelegate {
					visible: !coverOnSide

					cover: bookdata ? bookdata.cover : null
					fullcover: bookdata ? bookdata.fullcover : null

					anchors {
						horizontalCenter: parent.horizontalCenter
						topMargin: scaling.dp(5)
						bottomMargin: scaling.dp(4)
					}

					width: parent.width / 3
					height: parent.width / 2
					title: titleLabel.text
					author: bookdata ? bookdata.author : null
				}

				Column {
					width: parent.width
					spacing: scaling.dp(2)
					Label {
						text: gettext.tr('Description')
						font.bold: true
						width: parent.width
						horizontalAlignment: Text.AlignHCenter
						font.pixelSize: scaling.dp(13)
					}

					Label {
						id: descriptionLabel
						width: parent.width
						horizontalAlignment: Text.AlignLeft
						font.pixelSize: scaling.dp(13)
						wrapMode: Text.Wrap
					}
				}

				Rectangle { width: parent.width; height: 1; color: colors.item }

				Column {
					width: parent.width
					spacing: scaling.dp(2)
					Label {
						text: gettext.tr('Percorso file')
						font.bold: true
						width: parent.width
						horizontalAlignment: Text.AlignHCenter
						font.pixelSize: scaling.dp(13)
					}

					Label {
						id: filenameLabel
						width: parent.width
						horizontalAlignment: Text.AlignLeft
						font.pixelSize: scaling.dp(13)
						wrapMode: Text.WrapAnywhere
					}
				}

				Rectangle { width: parent.width; height: 1; color: colors.item }

				Column {
					width: parent.width
					spacing: scaling.dp(2)
					Label {
						text: gettext.tr('Author')
						font.bold: true
						width: parent.width
						horizontalAlignment: Text.AlignHCenter
						font.pixelSize: scaling.dp(13)
					}

					Label {
						id: authorLabel
						width: parent.width
						horizontalAlignment: Text.AlignLeft
						font.pixelSize: scaling.dp(13)
						wrapMode: Text.Wrap
					}
				}

				Rectangle { width: parent.width; height: 1; color: colors.item }

				Column {
					width: parent.width
					spacing: scaling.dp(2)
					Label {
						text: gettext.tr('Language')
						font.bold: true
						width: parent.width
						horizontalAlignment: Text.AlignHCenter
						font.pixelSize: scaling.dp(13)
					}

					Label {
						id: languageLabel
						width: parent.width
						horizontalAlignment: Text.AlignLeft
						font.pixelSize: scaling.dp(13)
						wrapMode: Text.Wrap
					}
				}

				Rectangle { width: parent.width; height: 1; color: colors.item }

				Column {
					width: parent.width
					spacing: scaling.dp(2)
					Label {
						text: gettext.tr('Subject')
						font.bold: true
						width: parent.width
						horizontalAlignment: Text.AlignHCenter
						font.pixelSize: scaling.dp(13)
					}

					Label {
						id: subjectLabel
						width: parent.width
						horizontalAlignment: Text.AlignLeft
						font.pixelSize: scaling.dp(13)
						wrapMode: Text.Wrap
					}
				}

				Rectangle { width: parent.width; height: 1; color: colors.item }

				SwipeControl {
					id: swipe
					anchors.margins: scaling.dp(20)

					/*/ A control can be dragged to delete a file.  The deletion occurs /*/
					/*/ when the user releases the control. /*/
					actionText: gettext.tr("Release to Delete")
					/*/ A control can be dragged to delete a file. /*/
					notificationText: gettext.tr("Swipe to Delete")
					onTriggered: {
						filesystem.remove(bookInfo.bookdata.filename)
						book_db.removeFile(bookInfo.bookdata.filename)

						localBooks.readBookDir();
						pageStack.pop();
					}
				}
			}
		}
	}
}

