/* Copyright 2013-2015 Robert Schroll
 * Copyright 2020-2021 Emanuele Sorce - emanuele.sorce@hotmail.com
 *
 * This file is part of Beru and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import "historystack.js" as History

import Browser 1.0

Page {
	id: bookPage

	property alias url: bookWebView.url
	property var currentChapter: null
	property var history: new History.History(updateNavButtons)
	property bool navjump: false
	property bool canBack: false
	property bool canForward: false
	property bool isBookReady: false
	property bool doPageChangeAsSoonAsReady: false

	property string book_componentId;
	property real book_percent;

	// picture (i.e. pdf) has limited options
	property bool pictureBook: server.reader.pictureBook;

	property int pdf_pageNumber: 0;
	property int pdf_numberOfPages: server.reader.numberOfPages();

	// book settings manager
	property BookSettings bookSettings: BookSettings { parent: bookPage }

	// outline
	property ListModel contentsListModel: ListModel {}
	property ListModel pagesTumblerModel: ListModel {}

	signal contentOpened()

	ContentDialog {
		id: content
	}

	Drawer {
		id: controls
		width: parent.width
		height: controlRect.height
		edge: Qt.BottomEdge
		modal: false

		Rectangle {
			id: controlRect

			antialiasing: false
			color: colors.background

			anchors.left: parent.left
			anchors.right: parent.right
			anchors.top: parent.top
			height: childrenRect.height
			width: parent.width

			// relaxed layout uses more space, nicer on wider screens
			// there is one button more on the right, so we check there
			property bool relaxed_layout: width * 0.5 >= jump_button.width + content_button.width + settings_button.width

			// reduce button size when even not relaxed layout not enought
			// 7 is the number of buttons
			// Not 100% accurate alghorithm, but this convers just edge cases (very small phone display)
			property int max_button_size: width / 7 - scaling.dp(1)

			FloatingButton {
				id: home_button
				anchors.left: parent.left
				max_size: controlRect.max_button_size
				buttons: [
					Action {
						icon.name: "go-home"
						onTriggered: {
							content.close();
							controls.close();
							closeFile();
						}
					}
				]
			}
			FloatingButton {
				id: history_button
				anchors.right: jump_button.left
				max_size: controlRect.max_button_size
				buttons: [
					Action {
						icon.name: "undo"
						enabled: canBack
						onTriggered: {
							var locus = history.goBackward()
							if (locus !== null) {
								navjump = true;
								moveToLocus(locus);
							}
						}
					},
					Action {
						icon.name: "redo"
						enabled: canForward
						onTriggered: {
							var locus = history.goForward()
							if (locus !== null) {
								navjump = true;
								moveToLocus(locus);
							}
						}
					}
				]
			}
			FloatingButton {
				id: jump_button
				anchors.right: content_button.left
				anchors.rightMargin: controlRect.relaxed_layout ? parent.width * 0.5 - content_button.width - settings_button.width - width : 0
				max_size: controlRect.max_button_size

				buttons: [
					Action {
						icon.name: "go-previous"
						onTriggered: {
							if(pictureBook)
								moveToLocus({ pageNumber: pdf_pageNumber - 10})
							else
								moveToLocus(-10, "moveToPageRelative");
						}
					},
					Action {
						icon.name: "go-next"
						onTriggered: {
							if(pictureBook)
								moveToLocus({ pageNumber: pdf_pageNumber + 10})
							else
								moveToLocus(10, "moveToPageRelative");
						}
					}
				]
			}
			FloatingButton {
				id: content_button
				anchors.right: settings_button.left
				max_size: controlRect.max_button_size
				buttons: [
					Action {
						icon.name: "book"
						onTriggered: {
							content.open();
							contentOpened();
							controls.close();
						}
					}
				]
			}
			FloatingButton {
				id: settings_button
				anchors.right: parent.right
				max_size: controlRect.max_button_size
				buttons: [
					Action {
						icon.name: "settings"
						onTriggered: {
							bookSettings.openDialog()
							controls.close();
						}
					}
				]
			}
		}
	}

    BusyIndicator {
        id: loadingIndicator
        width: scaling.dp(50)
        height: scaling.dp(50)
        anchors.centerIn: parent
        opacity: 1
        running: opacity != 0
    }

	Browser {
		id: bookWebView
		anchors.fill: parent
		opacity: 0

		onActiveFocusChanged: {
			if(activeFocus)
				controls.close();
		}

		// TODO: doesn't seem to work
		// TODO: RTL support
		Keys.onPressed: {
			if (event.key == Qt.Key_Right || event.key == Qt.Key_Down || event.key == Qt.Key_Space
					|| event.key == Qt.Key_Period) {
				if(pictureBook)
					moveToLocus(JSON.stringify({ pageNumber: pdf_pageNumber + 1}))
				else
					moveToLocus("+1", "moveToPageRelative");
				event.accepted = true;
			} else if (event.key == Qt.Key_Left || event.key == Qt.Key_Up
					|| event.key == Qt.Key_Backspace || event.key == Qt.Key_Comma) {
				if(pictureBook)
					moveToLocus(JSON.stringify({ pageNumber: pdf_pageNumber - 1}))
				else
					moveToLocus("-1", "moveToPageRelative");
				event.accepted = true;
			}
		}

		onWidthChanged: {
			updateRenderSize(width, height);
		}
		onHeightChanged: {
			updateRenderSize(width, height);
		}
		Connections {
			target: bookSettings
			onPdfQualityChanged: {
				updateRenderSize(width, height);
				if(server.reader.fileType == "PDF") {
					forcePageReloadPDF();
				}
			}
		}
	}

	// Below browser so that it get drawn over it
	Label {
		visible: pictureBook
		text: "" + (pdf_pageNumber == 0 ? "-" : pdf_pageNumber) + "/" + (pdf_numberOfPages == 0 ? "-" : pdf_numberOfPages)
		anchors.bottom: parent.bottom
		anchors.bottomMargin: scaling.dp(5)
		anchors.rightMargin: scaling.dp(5)
		anchors.right: parent.right
		font.pixelSize: scaling.dp(12)
		// TODO: page number is not visible if pdf page is of the same color on landscape
		color: bookSettings.infoColor
	}

	function forcePageReloadPDF() {
		bookWebView.runJavaScript("forcePageReload()");
	}

	function updateRenderSize(w, h) {
		if(server.reader.fileType == "PDF") {
			server.reader.setRenderMaxSize(w * bookSettings.pdfQuality, h * bookSettings.pdfQuality);
		}
	}

	// parse messages the html side running in the browser send us using the server.
	function parseApiCall( message ) {
		console.log("Book: " + message);

		var msg = message.split(" ");

		if(msg[0] == "Jumping") {
			bookPage.onJumping([JSON.parse(msg[1]), JSON.parse(msg[2])]);
		} else if(msg[0] == "UpdatePage") {
			if(!isBookReady) {
				doPageChangeAsSoonAsReady = true;
			} else {
				bookLoadingCompleted();
				bookPage.updateSavedPage();
			}
		} else if(msg[0] == "startLoading") {
			bookLoadingStart();
		} else if(msg[0] == "Ready") {
			isBookReady = true;
			if(doPageChangeAsSoonAsReady) {
				bookPage.updateSavedPage();
				doPageChangeAsSoonAsReady = false;
			}
			bookLoadingCompleted();
			controls.open();
		} else if(msg[0] == "status_requested") {
			bookWebView.runJavaScript("statusUpdate()");
		} else if(msg[0] == "chapter") {
			if(msg.length > 2)
				for(var i=2; i<msg.length; i++) msg[1] += " " + msg[i];
			currentChapter = JSON.parse(msg[1]);
		} else if(msg[0] == "percent") {
			book_percent = Number(msg[1]);
		} else if(msg[0] == "componentId") {
			book_componentId = msg[1];
		} else if(msg[0] == "pageNumber") {
			pdf_pageNumber = Number(msg[1]);
		} else if(msg[0] == "ok") {
			bookLoadingCompleted();
		} else if(msg[0] == "monocle:notfound") {
			// This is caused by some bug - we prevent the app from freeze in loading at least
			console.log("WARN: monocle:notfound has been fired. this could be a bug. Ignoring...")
			bookLoadingCompleted()
		} else if(msg[0] == "monocle:link:external") {
			var comp_id = msg[1].split("127.0.0.1:" + server.port + "/")[1];
			moveToLocus(comp_id, "moveToChapter")
		} else if(msg[0] == "request_number_of_pages") {
			bookWebView.runJavaScript("setNumberOfPages(" + pdf_numberOfPages + ")");
		} else if(msg[0] == "#") {}
		// not handled messages
		else console.log("ignored");
	}

	function bookLoadingStart() {
		bookWebView.opacity = 0;
		loadingIndicator.opacity = 1;
	}
	function bookLoadingCompleted() {
		bookWebView.opacity = 1;
		loadingIndicator.opacity = 0;
	}

	function moveToLocus(locus, func_name="moveToLocus") {
		bookLoadingStart();

		// manage more stuff in pdf / cbz
		if(pictureBook) {
			if(locus.pageNumber < 1)
				locus.pageNumber = 1;
			else if(locus.pageNumber > pdf_numberOfPages)
				locus.pageNumber = pdf_numberOfPages;

			if(Math.abs(locus.pageNumber - pdf_pageNumber) > 1)
				onJumping( [{pageNumber: pdf_pageNumber}, {pageNumber: locus.pageNumber}] )
		}
		var command = func_name + "(" + JSON.stringify(locus) + ")"
		bookWebView.runJavaScript(command);
	}

    function updateNavButtons(back, forward) {
        canBack = back
        canForward = forward
    }

    function parseContents(contents, level = 0) {
        if (level === undefined) {
            level = 0
			if(contentsListModel && contentsListModel.clear) contentsListModel.clear()
        }
        for (var i in contents) {
            var chp = contents[i]
            chp.level = level
        	contentsListModel.append(chp)
            if (chp.children !== undefined)
                parseContents(chp.children, level + 1)
        }
    }
    Connections {
		target: server.reader
		onContentsReady: {
			bookPage.parseContents(contents)
		}
	}

    function onJumping(locuses) {
        if (navjump)
            navjump = false
        else
            history.add(locuses[0], locuses[1])
    }

	function updateSavedPage() {
		setBookSetting("locus", {
			// monocle
			componentId: book_componentId,
			percent: Number(book_percent),
			// pdf
			pageNumber: pdf_pageNumber
		})
		pageMetric.turnPage()
	}

	// listen for pdf content ready
	Connections {
		target: server.reader
		onContentsReady: { // contents
			parseContents(contents);
		}
	}

	Component.onCompleted: {
		server.reader.contentsReady.connect(parseContents)
		bookSettings.loadForBook();
		bookSettings.update();

		if(pictureBook) {
			// pages
			pagesTumblerModel.clear();
			for (var i = 1; i <= pdf_numberOfPages; i += 1)
				pagesTumblerModel.append({"num": (i)});
			// manage toc from poppler
			server.reader.loadContents();
		}
	}
}
