/* Copyright 2013-2015 Robert Schroll
 *
 * This file is part of Beru and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 *
 * Copyright 2020 Emanuele Sorce
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GNU GPLv3. See the file COPYING for full details.
 */
import QtQuick 2.12
import QtQuick.Window 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.LocalStorage 2.0

import QtQuick.Controls.Material 2.5

import QtSystemInfo 5.0
import QtQuick.Window 2.2

import Metrics 1.0
import Importer 1.0

ApplicationWindow {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"
    id: mainView
    title: defaultTitle
    visible: true

    property string defaultTitle: "Sturm Reader"
	property var bookPageComponent: {
		var bp = Qt.createComponent("BookPage.qml");

		if (bp.status == Component.Error)
			console.log("Error loading component BookPage.qml:", bp.errorString());
		return bp;
	}
	property var bookPage: null
	property var headerTextSize: scaling.dp(19)


	// Keep screen on
	ScreenSaver {
		// TODO: the way to detect if we are reading a book is INACCURATE
		screenSaverEnabled: (!appsettings.displayon) && (pageStack.depth > 1)
	}

	// portable palette and default material theme colors
	Material.theme: Material.Dark
	Material.primary: Material.Red
	Material.accent: Material.DeepOrange
	Colors {
		id: colors
	}

	// for dp scaling support
	Scaling {
		id: scaling
	}

	// database management
	Database {
		id: book_db
	}

	DefaultCover {
		id: defaultCover
	}

	function openSettingsDatabase() {
		return LocalStorage.openDatabaseSync("BeruSettings", "1", "Global settings for Beru", 100000)
	}

	// Settings
    function getSetting(key) {
        var db = openSettingsDatabase()
        var retval = null
        db.readTransaction(function (tx) {
            var res = tx.executeSql("SELECT value FROM Settings WHERE key=?", [key])
            if (res.rows.length > 0)
                retval = res.rows.item(0).value
        })
        return retval
    }

    function setSetting(key, value) {
        var db = openSettingsDatabase()
        db.transaction(function (tx) {
            tx.executeSql("INSERT OR REPLACE INTO Settings(key, value) VALUES(?, ?)", [key, value])
        })
    }

    function getBookSetting(key) {
		if (server.reader.hash() == "") {
			console.log("Can't fetch key '" + key + "', hash is empty.");
			return undefined;
		}

		var settings = JSON.parse(getSetting("book_" + server.reader.hash()))
        if (settings == undefined)
            return undefined
        return settings[key]
    }

    function setBookSetting(key, value) {
        if (server.reader.hash() == "")
            return false

		databaseTimer.stack.push({
			hash: server.reader.hash(),
			key: key,
			value: value
		});
		databaseTimer.start();
		return true;
    }

	Timer {
		id: databaseTimer

		interval: 0
		repeat: false
		running: false
		triggeredOnStart: false
		property var stack: []
		onTriggered: {
			var first = stack.shift();

			if (first != undefined) {
				var settings = JSON.parse(getSetting("book_" + first.hash))
				if (settings == undefined)
					settings = {}
				settings[first.key] = first.value;
				setSetting("book_" + first.hash, JSON.stringify(settings))

				restart();
			}
        }
    }

	// Our own setting store since QSettings is unreliable
	onWidthChanged: { setSetting( "appconfig_width", width)}
	onHeightChanged: { setSetting( "appconfig_height", height)}
	onXChanged: { setSetting("appconfig_x", x)}
	onYChanged: { setSetting("appconfig_y", y)}
	QtObject {
		id: appsettings

		property alias sort: localBooks.sort
		onSortChanged: { setSetting( "appconfig_sort", appsettings.sort ); }

		property bool displayon
		function setDisplayOn(value) {
			displayon = value
			setSetting("appconfig_displayon", displayon ? "true": "false")
		}

		Component.onCompleted: {
			var csort = getSetting("appconfig_sort");
			var cwidth = getSetting("appconfig_width");
			var cheight = getSetting("appconfig_height");
			var cx = getSetting("appconfig_x");
			var cy = getSetting("appconfig_y");
			var cdisplayon = getSetting("appconfig_displayon");

			if(csort) appsettings.sort = csort;

			if(cwidth) mainView.width = cwidth;
			else mainView.width = 800;

			if(cheight) mainView.height = cheight;
			else mainView.height = 600;

			if(cx) mainView.x = cx;
			if(cy) mainView.y = cy;

			if(cdisplayon == "false") appsettings.displayon = false;
			else appsettings.displayon = true;
		}
	}

    StackView {
        id: pageStack
		anchors.fill: parent
        initialItem: localBooks
    }

	LocalBooks {
		id: localBooks
		visible: false
	}

	// Error
    Dialog {
		id: errorOpenDialog
		title: gettext.tr("Error Opening File")
		modal: true
		visible: false
		x: Math.round((parent.width - width) / 2)
        y: Math.round((parent.height - height) / 2)
		width: scaling.dp(300)
		height: scaling.dp(300)
		Label {
			width: scaling.dp(300)
			height: scaling.dp(300)
			text: server.reader.error
		}
		standardButtons: Dialog.Ok
	}

    Server {
        id: server
    }

	Importer {
		id: importer
		importPage: import_page
	}

	ImportPage {
		id: import_page
		visible: false
	}

	Metrics {
		id: pageMetric
	}

	function openConverter( file ) {
		pageStack.pop();
		var converter = pageStack.push("Converter.qml");
		converter.original_filename = file;

		// taken from ImportPage.qml - improve/modularize this
		var components = file.split("/").pop().split(".");
		var ext = components.pop();
		var dir = filesystem.getDataDir(localBooks.defaultdirname);
		var basename = components.join(".")
		var newfilename = basename + ".pdf"
		var i = 0
		while (filesystem.exists(dir + "/" + newfilename)) {
			i += 1
			newfilename = basename + "(" + i + ").pdf"
		}

		converter.converted_filename = dir + "/" + newfilename;
	}

	// close file
	function closeFile() {
		while (pageStack.currentItem != localBooks)
			pageStack.pop();
		mainView.title = mainView.defaultTitle;
		bookPage.destroy();
		bookPage = null;
	}

	// Open file
    function loadFile(filename) {
        if (server.reader.load(filename)) {
            while (pageStack.currentItem != localBooks)
                pageStack.pop()
			// create bookPage
			bookPage = bookPageComponent.createObject(pageStack, {url: "http://127.0.0.1:" + server.port + "/" + server.reader.fileType, visibile: false})
			pageStack.push(bookPage)
			mainView.title = server.reader.title()
			localBooks.updateRead(filename)
			return true
        }
        errorOpenDialog.open()
        return false
    }

	function forwardApiCall(call) {
		var i;
		for(i=0; i<pageStack.children.length; i++) {
			if(pageStack.children[i].id == "bookPage") {
				pageStack.children[i].parseApiCall(call);
				return;
			}
		}
		console.log("Error! Failed to delived api call!");
	}

    // Fonts
	FontLoader {
		source: Qt.resolvedUrl("../html/fonts/Bitstream Charter.ttf")
	}
	FontLoader {
		source: Qt.resolvedUrl("../html/fonts/URW Bookman L.ttf")
	}
	FontLoader {
		source: Qt.resolvedUrl("../html/fonts/URW Gothic L.ttf")
	}
	QtObject {
		id: fontLister

		property var fontList: ["Default", "Bitstream Charter", "Ubuntu", "URW Bookman L", "URW Gothic L"]

		Component.onCompleted: {
			var familyList = qtfontlist.families()

			var possibleFamilies = [["Droid Serif", "Nimbus Roman No9 L", "FreeSerif"],
									["Droid Sans", "Nimbus Sans L", "FreeSans"]]
			for (var j=0; j<possibleFamilies.length; j++) {
				for (var i=0; i<possibleFamilies[j].length; i++) {
					if (familyList.indexOf(possibleFamilies[j][i]) >= 0) {
						fontList.splice(2, 0, possibleFamilies[j][i])
						break
					}
				}
			}
		}
	}


    Component.onCompleted: {

		var db = openSettingsDatabase()
		db.transaction(function (tx) {
			tx.executeSql("CREATE TABLE IF NOT EXISTS Settings(key TEXT UNIQUE, value TEXT)")
		})

		// TODO: support for importing using args
		var bookarg = undefined //Qt.application.arguments[1]
		if (bookarg != undefined && bookarg != "" && bookarg != null)
		{
			var filePath = filesystem.canonicalFilePath(bookarg)
			if (filePath !== "") {
				if (loadFile(filePath))
					localBooks.addFile(filePath)
			}
		}

        localBooks.onMainCompleted();
    }
}
